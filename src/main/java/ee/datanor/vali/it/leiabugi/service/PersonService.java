package ee.datanor.vali.it.leiabugi.service;

import ee.datanor.vali.it.leiabugi.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Slf4j
@Service
public class PersonService {

    List<Person> personsList = new ArrayList<>();
    //    Logger log = new Logger();
    private static final Logger log = Logger.getLogger(PersonService.class.getName());

    @PostConstruct
    public void init() {
        Person firstP = new Person();
        firstP.setFirstName("Miki");
        firstP.setLastName("Hiir");
        firstP.setSocialSecurityId(31012340001L);

        Person secondP = new Person();
        secondP.setFirstName("Hagar");
        secondP.setLastName("Hirmus");
        secondP.setSocialSecurityId(34012340002L);

        Person thridP = new Person();
        thridP.setFirstName("Pipi");
        thridP.setLastName("Pikksukk");
        thridP.setSocialSecurityId(48012340003L);

        personsList.add(firstP);
        personsList.add(secondP);
        personsList.add(thridP);
        log.info("init done");
    }


    public Person getPerson(Long socialSecurityId) {

//        log.info("personList size {} {}", personsList.size(), socialSecurityId);
        log.info("Persooni tagastamine algas");

        Optional<Person> person = personsList.stream().filter((p -> p.getSocialSecurityId() == socialSecurityId)).findAny();
        if (person.isPresent()) {
            return person.get();
        }
        return new Person();
    }

    // Kristjan Oinus: lahendasin selliselt, et korduva koodi korral uut sissekannet ei tehta
    public void addPerson(Person person) throws IOException {
        long idToFind;
        boolean idExists;
        idToFind = person.getSocialSecurityId();
        idExists = personsList.stream().anyMatch(p -> p.getSocialSecurityId() == idToFind);
//        if (idExists = true) {
//            personsList.add(person);

        if (idExists == true) {
//            person.setSocialSecurityId(0000000000000);
//            personsList.add(person);
            throw new IOException("See isikukood on kasutusel. Ei saa inimest lisada.");
//            return;
        } else if (idExists == false) {
            person.setSocialSecurityId(person.getSocialSecurityId());
            personsList.add(person);
            return;
        }
    }

    public void editPerson(Person person) throws IOException {
        long idToFind;
        boolean idExists;
        String firstName = person.getFirstName();
        log.info("esinimi " + person.getFirstName());
        String lastName = person.getLastName();
        log.info("perenimi " + person.getLastName());

        idToFind = person.getSocialSecurityId();
        log.info("otsitav isikukood " + idToFind);
        idExists = personsList.stream().anyMatch(p -> p.getSocialSecurityId() == idToFind);
        log.info("leitud isikukood " + idExists);

        if (idExists == true) {

            System.out.println((String.valueOf(personsList.get(0).getSocialSecurityId())).equals(String.valueOf(person.getSocialSecurityId())));
            log.info("persoon nr " + personsList.get(0).getSocialSecurityId());
            for (int i = 0; i < personsList.size(); i++) {

                if ((String.valueOf(personsList.get(i).getSocialSecurityId())).equals(String.valueOf(person.getSocialSecurityId()))) {
                    personsList.get(i).setFirstName(firstName);

                    log.info("esinimi on nüüd " + personsList.get(i).getFirstName());
                    personsList.get(i).setLastName(lastName);

                    log.info("perenimi on nüüd " + personsList.get(i).getLastName());


                } else {
                    log.info("esinimi oleks nüüd " + personsList.get(i).getFirstName());
                }

            }

        } else if (idExists == false) {

            throw new IOException("Sellist isikukoodi ei esine");

//            return;
        }
    }

    public void deletePerson(Long socialSecurityId) {

//        log.info("personList size {} {}", personsList.size(), socialSecurityId);
        log.info("Kustutamine algas");

        Optional<Person> person = personsList.stream().filter((p -> p.getSocialSecurityId() == socialSecurityId)).findAny();
        if (person.isPresent()) {
            int a = getIndexByProperty(socialSecurityId);
            log.info(String.valueOf(a));
            personsList.remove(a);
        }
    }

    public List<Person> getPersons() {
        return personsList;
    }

     private int getIndexByProperty(long yourId) {
        for (int i = 0; i < personsList.size(); i++) {
            if (personsList.get(i) !=null && String.valueOf(personsList.get(i).getSocialSecurityId())
                    .equals(String.valueOf(yourId))) {
                return i;
            }
        }
        return -1;// not there is list
    }


}
